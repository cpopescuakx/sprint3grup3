<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
  <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
  <link href="fontawesome-free-5.11.2-web/css/all.css" rel="stylesheet">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="stylesheet" href="_footer.css">

  <footer class="pt-4 my-md-5 pt-md-5 border-top ">
    <div class="container ">
          <div class="row ">

            <div class="col-6 col-md">
                <h5>Sobre Proiectus</h5>
                <ul class="list-unstyled text-small">
                <li><a class="text-muted" href="#">Cool stuff</a></li>
                <li><a class="text-muted" href="#">Random feature</a></li>
                <li><a class="text-muted" href="#">Team feature</a></li>
                <li><a class="text-muted" href="#">Stuff for developers</a></li>
                <li><a class="text-muted" href="#">Another one</a></li>
                <li><a class="text-muted" href="#">Last time</a></li>
              </ul>
            </div>
            <div class="col-6 col-md">
              <h5>FAQ</h5>
              <ul class="list-unstyled text-small">
                <li><a class="text-muted" href="#">FAQ1</a></li>
                <li><a class="text-muted" href="#">FAQ2</a></li>
                <li><a class="text-muted" href="#">FAQiu</a></li>

              </ul>
            </div>
            <div class="col-6 col-md">
              <h5>Info</h5>
              <ul class="list-unstyled text-small">
                <li><a class="text-muted" href="#">Equip</a></li>
                <li><a class="text-muted" href="#">Ubicació</a></li>
                <li><a class="text-muted" href="#">Privacitat</a></li>
                <li><a class="text-muted" href="#">Termes</a></li>
              </ul>
            </div>
            <div class="col-6 col-md">
              <h5>Blog</h5>
              <ul class="list-unstyled text-small">
                <li><a class="text-muted" href="#">El nostre blog</a></li>

              </ul>
            </div>
          </div>

        </div>
        <div class="border-bottom  pb-5-md ">
        </div>
        <div class="container">
          <div class="row">
            <div class="col-12 col-md pt-3">
              <img class="mb-2" src="Icono_1.png" alt="" width="40px" height="40px">
              <small class="d-block mb-3 text-muted">&copy; 2019-2020</small>
            </div>
          <div class="col-6 col-md no-gutters">

            <ul class="list-group list-group-horizontal">
              <li href="#"class="list-group-item cursor-pointer"><i class="fab fa-twitter-square fa-2x" href="#"></i></li>
              <li href="#"class="list-group-item cursor-pointer"><i class="fab fa-instagram fa-2x" href="#"></i></li>
              <li href="#"class="list-group-item cursor-pointer"><i class="fab fa-linkedin fa-2x" href="#"></i></li>
              <li href="#"class="list-group-item cursor-pointer"><i class="fab fa-facebook-square fa-2x"href="#"></i></li>
            </ul>
          </div>

          </div>
        </div>


        </footer>
